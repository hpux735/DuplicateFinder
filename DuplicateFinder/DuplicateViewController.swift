//
//  DuplicateViewController.swift
//  DuplicateFinder
//
//  Created by William Dillon on 7/19/14.
//  Copyright (c) 2014 William Dillon. All rights reserved.
//

import Cocoa

class DuplicateViewController: NSViewController {
    var lib: IPhotoLib? = nil
    var master: DuplicateTester? = nil
    var missingImages = Array<IPhotoLib.Image>()
    
    var duplicates: NSArray! = NSArray()
    
    @IBOutlet var duplicateProgressBar: NSProgressIndicator? = nil
    @IBOutlet var masterProgressBar: NSProgressIndicator? = nil
    @IBOutlet var candidateProgressBar: NSProgressIndicator? = nil
    @IBOutlet var copyProgressBar: NSProgressIndicator? = nil
    
    @IBOutlet var masterTextField: NSTextField? = nil
    @IBOutlet var candidateTextField: NSTextField? = nil
    @IBOutlet var destinationTextField: NSTextField? = nil
    
    var countString: NSString {
    get {
//        if lib {
            return NSString(string: "Library not yet loaded.")
//        } else {
//            return NSString(string: "Loaded a library with \(lib!.images.count) images.")
//        }
    }
    set {
        return
    }}
    
    @IBAction func openLibraryDuplicate(sender: AnyObject) -> Void {

        // Request a Library from the user
        var fileWindow = NSOpenPanel()
        fileWindow.runModal()

        // Make sure that one was selected (this will fail if the user
        // clicked "cancel" for example.
        if let URL = fileWindow.URL {

            // In another thread, open the library.
            // This is necessary because this process is very time consuming
            // If we don't do this, the user will see the "spinning beach
            // ball of death" for a very long time.
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                var dup = DuplicateTester(master: IPhotoLib(path: URL.path)) {
                    (completed: Int, total: Int) -> Void in
                    if  self.duplicateProgressBar != nil {
                        self.duplicateProgressBar!.minValue = 0
                        self.duplicateProgressBar!.maxValue = Double(total)
                        self.duplicateProgressBar!.doubleValue = Double(completed)
                    }
                }
            }
        }
    }
    
    @IBAction func openLibraryMaster(sender: AnyObject) -> Void {

        // Request a Library from the user
        var fileWindow = NSOpenPanel()
        fileWindow.runModal()

        // Make sure that one was selected (this will fail if the user
        // clicked "cancel" for example.
        if let URL = fileWindow.URL {
            if masterTextField != nil { masterTextField!.stringValue = URL.lastPathComponent }
            
            // In another thread, open the library.
            // This is necessary because this process is very time consuming
            // If we don't do this, the user will see the "spinning beach
            // ball of death" for a very long time.
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                self.master = DuplicateTester(master: IPhotoLib(path: URL.path)) {
                    (completed: Int, total: Int) -> Void in
                    if  self.masterProgressBar != nil {
                        self.masterProgressBar!.minValue = 0
                        self.masterProgressBar!.maxValue = Double(total)
                        self.masterProgressBar!.doubleValue = Double(completed)
                    }
                }
            }
        }
    }
    
    @IBAction func openLibraryCandidate(sender: AnyObject) -> Void {
        if master != nil {

            var fileWindow = NSOpenPanel()
            fileWindow.runModal()

            if let URL = fileWindow.URL {
                if candidateTextField != nil { candidateTextField!.stringValue = URL.lastPathComponent }

                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    self.missingImages = self.master!.compare(IPhotoLib(path: URL.path)) {
                        (completed: Int, total: Int) -> Void in
                        if  self.candidateProgressBar != nil {
                            self.candidateProgressBar!.minValue = 0
                            self.candidateProgressBar!.maxValue = Double(total)
                            self.candidateProgressBar!.doubleValue = Double(completed)
                        }
                    }
                }
            }
        } else {
            println("Please select master library first.")
            var dialog = NSAlert()
            dialog.alertStyle  = NSAlertStyle.InformationalAlertStyle
            dialog.informativeText = "A master library must be selected before a Candidate library."
            dialog.messageText = "Select Master library first."
            dialog.runModal()
        }
    }
    
    @IBAction func copyMissingImages(sender: AnyObject) -> Void {
        if missingImages.count == 0 {
            println("No images to copy.")
            var dialog = NSAlert()
            dialog.alertStyle  = NSAlertStyle.InformationalAlertStyle
            dialog.informativeText = "There aren't any images to copy.  Either master and candidate libraries haven't been selected or the master library contains each image from the candidate library."
            dialog.messageText = "No images to copy."
            dialog.runModal()
            return
        }
        
        var fileWindow = NSOpenPanel()
        fileWindow.canChooseDirectories = true
        fileWindow.canCreateDirectories = true
        fileWindow.runModal()
        
        if let URL = fileWindow.URL {
            if (destinationTextField != nil) { destinationTextField!.stringValue = URL.path }

            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                var manager = NSFileManager()
                var error: NSError? = nil

                // Create a CSV of the images comment and captions
                var csvString: String = "\"File name\",\"Caption\",\"Comment\"\n"
                
                for (var i: Int = 0; i < self.missingImages.count; i++) {
                    var img = self.missingImages[i]

                    // Copy each image from the candidate library to the provided folder.
                    var imageURL = NSURL(fileURLWithPath: img.imagePath)
                    var imageFileName = imageURL.lastPathComponent
                    imageFileName = imageFileName.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
                    var tempURL: NSURL? = NSURL(string: imageFileName, relativeToURL: URL)
                    if let destURL = tempURL? {
                        // Make sure the file doesn't exist
                        if !manager.fileExistsAtPath(destURL.path) {
                            if manager.copyItemAtURL(imageURL, toURL: destURL, error: &error) == false {
                                
                                // There was some problem with the copy.  Let the user know.
                                var alert = NSAlert(error: error)
                                alert.runModal()
                                return
                                
                            }
                        }

                        // Update the progress bar
                        if  (self.copyProgressBar != nil) {
                            self.copyProgressBar!.minValue = 0
                            self.copyProgressBar!.maxValue = Double(self.missingImages.count)
                            self.copyProgressBar!.doubleValue = Double(i)
                        }

                        csvString += "\"\(imageURL.lastPathComponent)\",\"\(img.caption)\",\"\(img.comment)\"\n"

                    } else {
                        var alert = NSAlert()
                        alert.alertStyle  = NSAlertStyle.InformationalAlertStyle
                        alert.informativeText = "Unable to create file because the path couldn't be constructed.  Base path: \(URL.path) File name: \(imageURL.lastPathComponent)"
                        alert.messageText = "Unable to create file"
                        alert.runModal()
                    }
                }
                
                // Save the CSV file to the folder
                var dest = NSURL(string: "missingImages.csv", relativeToURL: URL)
                if !csvString.writeToURL(dest, atomically: false, encoding: NSUTF8StringEncoding, error: &error) {
                    var alert = NSAlert()
                    alert.alertStyle  = NSAlertStyle.InformationalAlertStyle
                    alert.informativeText = "Unable to create spreadsheet of captions and comments"
                    alert.messageText = "Unable to create file"
                    alert.runModal()
                }
            }
        }
    }
}