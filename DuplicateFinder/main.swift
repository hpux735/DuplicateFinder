//
//  main.swift
//  DuplicateFinder
//
//  Created by William Dillon on 7/13/14.
//  Copyright (c) 2014 William Dillon. All rights reserved.
//

import Cocoa

NSApplicationMain(C_ARGC, C_ARGV)
