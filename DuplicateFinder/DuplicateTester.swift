//
//  DuplicateTester.swift
//  DuplicateFinder
//
//  Created by William Dillon on 7/13/14.
//  Copyright (c) 2014 William Dillon. All rights reserved.
//

import Cocoa

extension String {
    subscript (i: Int) -> Character {
        return Array(self)[i]
    }
    
    func contains(char: Character) -> Bool {
        for (var i = 0; i < countElements(self); i++) {
            if char == self[i] { return true }
        }
        
        return false
    }
}

class HashCache {
    struct FileInstance {
        let hash:  String
        let path:  String
        let size:  Int
    }

    @objc class stringAppender {
        var objcString: NSMutableString! = nil
        
        init(size: Int = 0) {
            objcString = NSMutableString.stringWithCapacity(size)
        }
        
        func append(string: NSString) -> Void {
            objcString.appendFormat("%@", string)
        }
        
        func getString(Void) -> String {
            return objcString
        }
    }
    
    class StringSplitter {
        class func instancesFromFile(path: String) -> [String:FileInstance]? {
            var instances = [String:FileInstance]()
            var err: NSError?
            let src = NSString.stringWithContentsOfFile(path, encoding: NSUTF16StringEncoding, error: &err)
            if let err = err {
                //println("Could not load file \(path): \(err.description)")
                return nil
            }
            
            let regex = NSRegularExpression
                .regularExpressionWithPattern( "^(.*)\t(.*)\t(.*)$",
                    options: NSRegularExpressionOptions.AnchorsMatchLines,
                    error: &err)
            
            var line = 0
            var success = true
            
            regex.enumerateMatchesInString(
                src,
                options: NSMatchingOptions(0),
                range: NSRange(location:0,length:src.length),
                usingBlock: { result, flags, stop in
                    line++
                    let count = result.numberOfRanges
                    if count == 4 {
                        let hash    = src.substringWithRange(result.rangeAtIndex(1))
                        let path    = src.substringWithRange(result.rangeAtIndex(2))
                        let sizeStr = src.substringWithRange(result.rangeAtIndex(3))
                        if let size = sizeStr.toInt() {
                            instances[path] = FileInstance(hash: hash, path: path, size: size)
                        } else {
                            println("Wrong type of size at line #\(line), file: \(path)")
                            success = false
                        }
                    } else {
                        println("Wrong number of columns at line #\(line), file: \(path)")
                        success = false
                    }
                    if !success { /* break enumerate loop */ stop.memory = true; return }
                    
            })
            return success ? instances : nil
        }
    }

    class func getApplicationSupportPath(Void) -> String {
        var manager = NSFileManager()
        var urls = manager.URLsForDirectory(NSSearchPathDirectory.ApplicationSupportDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask)
                
        return urls[0].path + "/DuplicateTester/hashcache.tsv"
    }
    
    var lock = NSLock()
    var instances = Dictionary<String, FileInstance>()

    init(path: String) {
        if let temp = StringSplitter.instancesFromFile(path)? {
            instances = temp
            println("Loaded \(instances.count) entries from the hash cache.")
        } else {
            println("Starting with an empty hash cache.")
        }

//        var error: NSError? = nil
//        var cache = String.stringWithContentsOfFile(path, encoding: NSUTF16StringEncoding, error: &error)
//        if cache? == nil || error? != nil {
//            println("Error while opening cache file (\(error!.localizedFailureReason)): \(path)")
//            return
//        }
//
//        // Process each file line in turn
//        for line in cache!.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet()) {
//            var seperator: NSCharacterSet = NSCharacterSet(charactersInString: "\t")
//            var columns: [String] = line.componentsSeparatedByCharactersInSet(seperator)
//
//            if columns.count < 3 {
//                println("Malformed line found: \(line)")
//                continue
//            }
//
//            if let size = columns[2].toInt() {
//                var fileInstance = FileInstance(hash: columns[0],
//                    path: columns[1], size: size)
//
//                instances[fileInstance.path] = fileInstance
//            } else {
//                println("Malformed line found: \(line)")
//                continue
//            }
//        }
        
    }

    func save(path: String, callback: (Int, Int) -> Void = {(one: Int, two: Int) -> Void in return}) {
        // Only do one save at a time
        if lock.tryLock() {

            var size = 0
            
            // Attempt to calculate how big the string will be
            for instance in instances.values {
                size += countElements(instance.hash) + 1
                size += countElements(instance.path) + 1
                size += countElements("\(instance.size)") + 1
            }
            
            println("Estimated hash cache size: \(size)")
            
            var appender = stringAppender(size: size)
            var counter = 0
            for instance in instances.values {
                
                if instance.path.isEmpty || instance.hash.isEmpty {
                    continue
                }
                
                var temp: NSString = instance.path
                if temp.containsString("\t") {
                    //println("Path \"\(instance.path) contains a tab, skipping")
                    continue
                }

                var nsString = NSString(format: "%@\t%@\t%d\n", instance.hash, instance.path, instance.size)
                
                appender.append(nsString)
                
                counter += 1
                callback(counter, instances.count)
            }

            var string = appender.getString()
            
            println("Actual hashcache size: \(countElements(string))")
                
            var error: NSError? = nil
            string.writeToFile(path, atomically: false, encoding: NSUTF16StringEncoding, error: &error)
            if error != nil {
                println("Error writing string: \(error?.localizedDescription)")
                lock.unlock()
                return
            }
            
            println("Saved \(instances.count) hashes to the cache.")
            lock.unlock()
        }
    }
    
    func getHashForPath(path: String) -> String {
        if path.isEmpty { return "" }
        
        // First check the instances dictionary for the file
        if let instance = instances[path] {
            return instance.hash
        }
        
        // Otherwise, generate a new hash
        else {
            var hashString = ""
            
            autoreleasepool() {
                let fileData = NSData(contentsOfFile: path)
                
                if fileData.length != 0 {
                    let hashBytes = UnsafeMutablePointer<UInt8>(malloc(UInt(CC_MD5_DIGEST_LENGTH)))
                    CC_MD5(fileData.bytes, UInt32(fileData.length), hashBytes)
                    let hashData = NSData(bytesNoCopy: hashBytes, length: Int(CC_MD5_DIGEST_LENGTH))
                    free(hashBytes)
                    
                    hashString = hashData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(0))

                    // Add the new file to the hash cache
                    self.instances[path] = FileInstance(hash: hashString, path: path, size: fileData.length)
                } else {
                    println("Unable to open file to hash: \(path)")
                }
            }
        
            return hashString
        }
    }
}

let cachePath: String = HashCache.getApplicationSupportPath() //NSBundle.mainBundle().resourcePath + "/hashCache.tsv"
let sharedHashCache = HashCache(path: cachePath)

class DuplicateTester {
    let masterLib:  IPhotoLib
    let hashCache = sharedHashCache
    
    var masterHash: Dictionary<String, IPhotoLib.Image>
    
    var duplicateArrayMaster = Array<(IPhotoLib.Image, IPhotoLib.Image)>()
    var duplicateArrayCheck  = Array<IPhotoLib.Image>()

    init(master: IPhotoLib, callback: (Int, Int) -> Void = {(one: Int, two: Int) -> Void in return}) {
        masterLib = master
        
        masterHash = Dictionary<String, IPhotoLib.Image>()
        
        println("Building hash library, this may take some time.")
        var images = Array<IPhotoLib.Image>(master.images.values)
        for (var i: Int = 0; i < images.count; i++) {
            var img = images[i]
            let hash = hashCache.getHashForPath(img.imagePath)

            if !hash.isEmpty {
                if let orig = masterHash[hash]? {
                    var entry = (img, orig)
                    duplicateArrayMaster += [entry]
                } else {
                    masterHash[hash] = img
                }
            }
            
            callback(i, images.count)
        }
        
        // Save the hash cache on another thread
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.hashCache.save(cachePath, callback)
        }
        
        if duplicateArrayMaster.count > 0 {
            println("Found \(duplicateArrayMaster.count) duplicates:")

            duplicateArrayMaster.sort() { a, b -> Bool in
                let (an, ao) = a
                let (bn, bo) = b
                return ao.caption < bo.caption
            }
            
            for (dup, orig) in duplicateArrayMaster {
                println("\(orig.caption) == \(dup.caption) in \(dup.albums.count) album(s):")
                for album in dup.albums {
                    println("\t\(album.name)")
                }
            }
        }
    }
    
    func compare(other: IPhotoLib, callback: (Int, Int) -> Void = {(one: Int, two: Int) -> Void in return}) -> Array<IPhotoLib.Image> {
        // Make a copy of the other librarys hash array, and remove the entries
        // that exist in the master.
        var testHash = Dictionary<String, IPhotoLib.Image>()

        var images = Array<IPhotoLib.Image>(other.images.values)
        for (var i: Int = 0; i < images.count; i++) {
            var img = images[i]

            let hash = hashCache.getHashForPath(img.imagePath)
            
            if !hash.isEmpty {
                if let orig = testHash[hash]? {
                } else {
                    testHash[hash] = img
                }
            }
            
            callback(i, images.count)
        }
        
        // Save the hash cache on another thread
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.hashCache.save(cachePath, callback)
        }
        
        for (key, value) in masterHash {
            testHash.removeValueForKey(key)
        }
        
        // When we're finished, the remaining items are missing from the master
        for (key, value) in testHash {
            println("Found an asset missing from the master library: \(value.imagePath)")
        }
        
        return Array<IPhotoLib.Image>(testHash.values)
    }
    
    func findOrphans() -> Array<String> {
        return Array<String>()
    }
}
