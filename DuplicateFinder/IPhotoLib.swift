//
//  IPhotoLib.swift
//  DuplicateFinder
//
//  Created by William Dillon on 7/13/14.
//  Copyright (c) 2014 William Dillon. All rights reserved.
//

import Foundation

class IPhotoLib {
    class Image {
        let key:       Int     = -1
        let caption:   String  = ""
        let comment:   String  = ""
        let GUID:      String  = ""
        let imagePath: String  = ""
        let thumbnail: String  = ""
        var albums:    Array<Album> = Array<Album>()

        init(xml: NSXMLElement, key newKey: Int) {
            key = newKey
            // Process the array of key, dict pairs in the master file list
            if let children: [NSXMLElement] = xml.children as? [NSXMLElement] {
                for (var i: Int = 0; i < children.count; i++) {
                    let name:    String = children[i].name
                    let strVal:  String = children[i].stringValue
                    let content: String = children[i+1].stringValue
                    let other:   String = children[i+1].name
                    i++
                    
                    if name == "key" {
                        switch strVal {
                        case "Caption":      caption   = content
                        case "Comment":      comment   = content
                        case "GUID":         GUID      = content
                        case "ThumbPath":    thumbnail = content
                        case "OriginalPath": imagePath = content
                        // Only set the image path if it isn't already set.
                        // With movies, the original path is the right one (I think)
                        case "ImagePath": if imagePath.isEmpty { imagePath = content }
                        default: continue
                        }
                    }
                }
            }
        }
        
        func addAlbum(album: Album) {
            albums.append(album)
        }
    }
    
    class Album {
        let id:     Int     = -1
        let name:   String  = ""
        let type:   String  = ""
        let GUID:   String  = ""
        let master: Bool    = false
        var images: [Image] = [Image]()
        
        init(xml: NSXMLElement, lib: IPhotoLib) {
            // Process the array of key, dict pairs in the master file list
            if let children: [NSXMLElement] = xml.children as? [NSXMLElement] {
                for (var i: Int = 0; i < children.count; i++) {
                    let xmlName: String = children[i].name
                    let strVal:  String = children[i].stringValue
                    i++
                    let content: String = children[i].stringValue
                    let other:   String = children[i].name
                    
                    if xmlName == "key" {
                        switch strVal {
                        case "AlbumId":      id        = content.toInt()!
                        case "AlbumName":    name      = content
                        case "Album Type":   type      = content
                        case "GUID":         GUID      = content
                        case "Master":       master    = true
                        case "KeyList":
                            if let keys = children[i].children as? [NSXMLElement] {
                                for key in keys {
                                    if let numericKey = key.stringValue.toInt() {
                                        if var image = lib.images[numericKey] {
                                            images.append(image)
                                        } else {
                                            println("Album named \(name) called for an image not in master list \(numericKey)")
                                        }
                                    } else {
                                        println("Album key not convertable to an int \(key.stringValue)")
                                    }
                                }
                            }
                        default: continue
                        }
                    }
                }
            }
        }
    }
    
    let images: Dictionary<Int, Image> = Dictionary<Int, Image>()
    let albums: [Album] = [Album]()
    let basePath: String = ""
    let library: String = ""
    var missing: Array<String> = Array<String>()

    private class func generateImageArray(xml: NSXMLElement, callback: (Int, Int) -> Void = {(one: Int, two: Int) -> Void in return}) -> Dictionary<Int, Image> {
        var dict: Dictionary<Int, Image> = Dictionary<Int, Image>()
        var key: Int = -1
        
        // Process the array of key, dict pairs in the master file list
        if let children = xml.children as? [NSXMLElement] {
            for (var i = 0; i < children.count; i++) {
                let child = children[i]
                let name: NSString = child.name
                
                switch name {
                case "key":
                    key = child.stringValue.toInt()!
                case "dict":
                    dict[key] = Image(xml: child, key: key)
                default:
                    println("Invalid element found in master file list: \(name)")
                }
                
                callback(i, xml.children.count)
            }
        }
        
        println("Found \(dict.count) assets.")
        
        return dict
    }
    
    private func generateAlbumArray(xml: NSXMLElement) -> Array<Album> {
        var array: [Album] = [Album]()
        
        if let children = xml.children as? [NSXMLElement] {
            for child in children {
                let album = Album(xml: child, lib: self)
                
                // We're not interested in the "master" album
                if album.master == false {
                    // Or special month albums
                    if album.type != "Special Month" {
                        array.append(album)
                    }
                }
            }
        }
        
        for album in array {
            for (var i = 0; i < album.images.count; i++) {
                var image = album.images[i]
                image.addAlbum(album)
            }
        }
        
        return array
    }
    
    init(path: String, callback: (Int, Int) -> Void = {(one: Int, two: Int) -> Void in return}) {
        // The provided path is the library base path
        basePath = path

        var aperatureLib = "/ApertureData.xml"
        var iPhotoLib = "/AlbumData.xml"

        // Search for either AperatureData.xml or AlbumData.xml
        var manager = NSFileManager.defaultManager()
        println("Checking for file \(basePath + aperatureLib)")
        println("Checking for file \(basePath + iPhotoLib)")
        
        if manager.fileExistsAtPath(basePath + aperatureLib) {
            library = aperatureLib
        } else if manager.fileExistsAtPath(basePath + iPhotoLib) {
            library = iPhotoLib
        } else {
            println("Can't find a compatible library.")
            library = ""
            images = Dictionary<Int, Image>()
            return
        }
        
        println("Opening \(basePath + library)")

        // Scan the Masters folder for images, and make a dict
        // with nothing really in it.  Then, as we process the XML file,
        // remove the entries as we find them.  This will ensure that there
        // are not images that are missed when we scan the XML file.
        println("Path: \(path)")
//        var filesDict  = Dictionary<String, String>()
//        var mastersPath = path + "/Masters/"
//        var enumerator = manager.enumeratorAtPath(mastersPath)
        
        // The nextObject has to be downcast to a string
//        var file: String! = enumerator.nextObject() as? String
//        while file != nil {
//            // Ignore directories
//            if  !file.pathExtension.isEmpty {
//                var filePath = mastersPath + file
//                filesDict[filePath.uppercaseString] = filePath
//            }
//            
//            file = enumerator.nextObject() as? String
//        }
//
//        println("Found \(filesDict.count) files in the Masters subtree.")
        
        let URL  = NSURL(fileURLWithPath: basePath + library)
        let xmlRep = NSXMLDocument(contentsOfURL: URL, options: 0, error: nil)
        
        // The root of the whole thing is a single node named 'dict'
        var children: [AnyObject] = xmlRep.rootElement().children
        var root: NSXMLElement! = nil
        
        if let tempRoot = children[0] as? NSXMLElement {
            root = tempRoot
        } else {
            println("Root element not an NSXMLElement!")
            exit(1)
        }
        
        var masterRoot: NSXMLElement! = nil
        var facesRoot:  NSXMLElement! = nil
        var albumsRoot: NSXMLElement! = nil
        var rollsRoot:  NSXMLElement! = nil

        //println("Root name \(root.name)")
        //println("Root children: \(root.children.count)")
        
        if let children: [NSXMLElement] = root.children as? [NSXMLElement] {
            for (var i = 0; i < children.count; i++) {
                let child = children[i]
                let strVal: NSString = child.stringValue

                // If the element name is "key" then the element
                // we want to keep is the next one.
                if child.name == "key" {
                    switch strVal {
                    case "List of Albums":
                        println("Found album list.")
                        albumsRoot = children[i + 1]
                        i++
                    case "List of Rolls":
                        println("Found rolls list.")
                        rollsRoot = children[i + 1]
                        i++
                    case "List of Faces":
                        println("Found faces list.")
                        facesRoot = children[i + 1]
                        i++
                    case "Master Image List":
                        println("Found master image list.")
                        masterRoot = children[i + 1]
                        i++
                    default:
                        println("Ignoring \(child.name) \(strVal)")
                        continue
                    }
                }
            }
        } else {
            println("Grandchild not an NSXMLElement!")
        }

        // Process the master file list
        if masterRoot != nil {
            images = IPhotoLib.generateImageArray(masterRoot, callback)
        } else {
            println("Library doesn't appear to have a valid master file list.")
            exit(1)
        }
        
        // Next, process the albums list
        if albumsRoot != nil {
            albums = generateAlbumArray(albumsRoot)
        }
        
        // Finally, check whether the any of the images actually got a album
//        for (key, image) in images {
//            println("Image \(image.caption) is in \(image.albums.count) albums:")
//            for album in image.albums {
//                println("Album named \(album.name) with \(album.images.count) images")
//            }
//        }

//        for album in albums {
//            println("Album named \(album.name) with \(album.images.count) images")
//            for image in album.images {
//                println("\t\(image.caption), \(image.albums.count)")
//            }
//        }
        
        // Finally, process the files dictionary, and remove all the images
        // that are in the XML file.
//        for img: IPhotoLib.Image in images.values {
//            filesDict.removeValueForKey(img.imagePath.uppercaseString)
//        }
        
        // Print some information about the files remaining in the dict
//        println("\(filesDict.count) files found masters that aren't in the library:")
//        self.missing = Array<String>(filesDict.values)
    }
}
