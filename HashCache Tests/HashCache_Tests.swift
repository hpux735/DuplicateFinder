//
//  HashCache_Tests.swift
//  HashCache Tests
//
//  Created by William Dillon on 7/27/14.
//  Copyright (c) 2014 William Dillon. All rights reserved.
//

import Cocoa
import XCTest

class HashCache_Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        var cache = sharedHashCache
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testHashCache() {
        var cache = sharedHashCache
        
        cache.save(cachePath)
        
//        assert(cache.instances.count > 0, "Loaded hashCache had no instances")
    }
    
    func testHashCacheInitTime() {
        self.measureBlock() {
            let cache = HashCache(path: "/Users/wdillon/Documents/Source Code/imaging/DuplicateFinder/HashCache Tests/hashCache.tsv")
        }
    }
    
    func testHashCacheSaveTime() {
        var cache = sharedHashCache
        self.measureBlock() {
            cache.save(cachePath)
        }
    }
}
